import { User } from "../User";

const u: User = User.deserialize(localStorage.getItem("userInfo"));

window.onload = () => {
  // replace everything
  // between the <a ...> </a> tags
  document.getElementById("subdomain").innerHTML = u.society;

  document.getElementById("name").innerHTML = u.name;
  if (!u.duesPaid) {
    document.getElementById("duespaid").innerHTML =
      "You haven't paid your dues yet!";
  }

  if (u.absencesLeft === 0) {
    document.getElementById("remainingAbsences").innerHTML = "no";
  } else {
    document.getElementById(
      "remainingAbsences",
    ).innerHTML = u.absencesLeft.toString();
  }

  document.getElementById("missedMeetings").innerHTML = u.missedMeetingsAsHTML;

  document.getElementById(
    "remainingService",
  ).innerHTML = u.serviceLeft.toString();

  document.getElementById("completedEvents").innerHTML =
    u.completedEventsAsHTML; // creates a unordered list in HTML

  if (u.probation) {
    document.getElementById(
      "probation",
    ).innerHTML = `Note: You're under probation. Repeating a probationable offense is grounds for expulsion.`;
  }

  document.querySelector(".btn-signout").addEventListener("click", () => {
    document.cookie = "id=NONE;path=/";
    localStorage.setItem("userInfo", "");
    window.location.replace("../index.html");
  });
};
