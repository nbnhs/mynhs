import { RELAY_URL, SITE_NAME } from "../constants";
import getCookie from "../getCookie"; // getCookie.ts
import getUser from "../getUser"; // getUser.ts
import { Society, User } from "../User"; // User.ts

window.onload = () => {
  document.getElementById("site-name").innerHTML = SITE_NAME;
};

const ID = getCookie("id");
const first = getCookie("first");
const subdomain = getCookie("subdomain");
let society: Society;
switch (subdomain) {
  case "default": {
    society = "National Honor Society";
    break;
  }
  case "sci": {
    society = "Science National Honor Society";
    break;
  }
  case "math": {
    society = "Mu Alpha Theta";
    break;
  }
  default: {
    break;
  }
}

console.log(`ID: ${ID}, first: ${first}, domain: ${subdomain}`);

/**
 * Note for those not fluent:
 * Promises are JavaScript's way of implementing asynchronous functions.
 *
 * When a function is asynchronous, it is run in a new thread,
 * so the rest of the code can continue while the function does whatever it wants in the meantime.
 * Promises return their eventual values when you call .then() (or, in a function marked async, when you use "await")
 */

// The user's attendance
const roll: Promise<Response> = fetch(
  `${RELAY_URL}?get=roll&id=${ID}&first=${first}&subdomain=${subdomain}`,
);

// The user's service events
const service: Promise<Response> = fetch(
  `${RELAY_URL}?get=service&id=${ID}&first=${first}&subdomain=${subdomain}`,
);

// The dates of all possible meetings (will be matched later with user's attendance)
const meetingDates: Promise<Response> = fetch(
  `${RELAY_URL}?get=meetingDates&id=${ID}&first=${first}&subdomain=${subdomain}`,
);

// The names of all possible service events (will be matched later with user's completed service)
const eventNames: Promise<Response> = fetch(
  `${RELAY_URL}?get=eventNames&id=${ID}&first=${first}&subdomain=${subdomain}`,
);

const user = getUser(first, society, roll, service, meetingDates, eventNames);

user.then((u: User) => {
  localStorage.setItem("userInfo", u.serialized); // puts the user's information inside the browser's local storage
  window.location.replace("../app/index.html"); // redirects to website/app
});
